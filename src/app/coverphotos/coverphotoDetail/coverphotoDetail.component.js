(function (angular) {
  'use strict';

  angular
    .module('app.coverphotos')
    .component('coverphotoDetail', coverphotoDetail());

  function coverphotoDetail() {
    var component = {
      templateUrl: '/app/coverphotos/coverphotoDetail/coverphotoDetail.component.html',
      controller: coverphotoDetailController
    };
    return component;
  }

  coverphotoDetailController.$inject = [
    '$routeParams',
    'coverphotosService'
  ];

  function coverphotoDetailController($routeParams, coverphotosService) {
    var ctrl = this;
    ctrl.$onInit = onInit;

    function onInit() {
    ctrl.edit    = $routeParams.edit;
    
        coverphotosService.getcoverphoto($routeParams.coverphotoId)
          .then(function (data) {
            ctrl.coverphoto = data;
        });
    }

  }
})(window.angular);
