(function (angular) {
    'use strict';

    angular
        .module('app.coverphotos')
        .component('coverphotoNew', coverphotoNew());

    function coverphotoNew() {
        var component = {
            templateUrl: '/app/coverphotos/coverphotoNew/coverphotoNew.component.html',
            controller: coverphotoNewController,
        };
        return component;
    }
    
    coverphotoNewController.$inject = [
        'coverphotosService',
        '$location',
    ];
    
    function coverphotoNewController(coverphotosService,$location) {
        var ctrl        = this;
        ctrl.$onInit    = onInit;
        ctrl.newForm    = newForm;

        function onInit() {
            /*
            Campo ID no se muestra en formulario, ya que para evitar choques
            con otros IDs deberá asignarlo el sistema remoto.
            */
            ctrl.coverphoto      = {
                ID           :0, 
                IDBook       :0,
                Url    :'',
            }
        }

        function newForm(){
            coverphotosService.newcoverphoto(ctrl.coverphoto)
                .then(function (data) {
                    alert ("El elemento fue creado correctamente, se muestra en consola la respuesta");
                    console.log(data);
                    $location.path('/coverphotos');
            });
        }

    }
})(window.angular);
