(function (angular) {
    'use strict';

    angular
        .module('app.coverphotos')
        .component('coverphoto', coverphoto());

    function coverphoto() {
        var component = {
            templateUrl: '/app/coverphotos/coverphoto/coverphoto.component.html',
            controller: coverphotoController,
            bindings: {
                coverphoto: '<',
                edit: '<',
            }
        };
        return component;
    }

    coverphotoController.$inject = [
        '$routeParams',
        'coverphotosService',
        '$location',
    ];

    function coverphotoController($routeParams,coverphotosService,$location) {
        var ctrl        = this;
        ctrl.$onInit    = onInit;
        ctrl.editForm   = editForm;
    
        function onInit() {
            coverphotosService.getcoverphoto($routeParams.coverphotoId)
                .then(function (data) {
                //ctrl.date  = new Date(data.PublishDate);
            });
        }

        function editForm(){
            //ctrl.author.PublishDate   = ctrl.date;

            coverphotosService.editcoverphoto(ctrl.coverphoto)
                .then(function (data) {
                    alert ("El elemento fue editado correctamente, se muestra en consola la respuesta");
                    console.log(data);
                    $location.path('/coverphotos');
            });
        }
    }
})(window.angular);
