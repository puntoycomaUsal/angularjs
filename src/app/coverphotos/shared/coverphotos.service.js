(function (angular) {
  'use strict';

  angular
    .module('app.coverphotos')
    .factory('coverphotosService', coverphotosService);

  coverphotosService.$inject = [
    '$http'
  ];

  function coverphotosService($http) {
    var urlRoot     = 'https://fakerestapi.azurewebsites.net/';
    var urlRootApi  = 'https://fakerestapi.azurewebsites.net/api/';

    var service = {
      getcoverphoto     : getcoverphoto,
      getcoverphotos    : getcoverphotos,
      deletecoverphoto  : deletecoverphoto,
      editcoverphoto    : editcoverphoto,
      newcoverphoto     : newcoverphoto,
    };

    return service;

    function getcoverphotos() {
      return $http.get(urlRootApi + 'CoverPhotos')
        .then(complete)
        .catch(failed);
    }

    function getcoverphoto(coverphotoId) {
      return $http.get(urlRootApi + 'CoverPhotos/' + coverphotoId)
        .then(complete)
        .catch(failed);
    }

    function deletecoverphoto(coverphotoId){
        return $http.delete(urlRootApi + 'CoverPhotos/' + coverphotoId)
            .then(complete)
            .catch(failed);
    }

    function editcoverphoto(coverphoto){
        return $http.put(urlRootApi + 'CoverPhotos/' +coverphoto.ID,coverphoto)
            .then(complete)
            .catch(failed);
    }

    function newcoverphoto(coverphoto){
        return $http.post(urlRootApi + 'CoverPhotos/',coverphoto)
            .then(complete)
            .catch(failed);
    }

    function complete(response) {
      return response.data;
    }

    function failed(error) {
      console.error(error.data);
    }
  }
})(window.angular);
