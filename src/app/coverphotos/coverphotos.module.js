(function (angular) {
  'use strict';

  angular
    .module('app.coverphotos', [
        'app.core',
    ]);
})(window.angular);
