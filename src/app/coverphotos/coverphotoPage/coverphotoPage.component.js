(function (angular) {
    'use strict';

    angular
        .module('app.coverphotos')
        .component('coverphotoPage', coverphotoPage());

    function coverphotoPage() {
        var component = {
            templateUrl: '/app/coverphotos/coverphotoPage/coverphotoPage.component.html',
            controller: coverphotoPageController
        };
        return component;
    }

    coverphotoPageController.$inject = [
        'coverphotosService',
    ];

    function coverphotoPageController(coverphotosService) {
        var ctrl        = this;
        ctrl.onFilter   = onFilter;
        ctrl.onOrderBy  = onOrderBy;
        ctrl.$onInit    = onInit;

        function onFilter(filter) {
            ctrl.filter = filter;
        }

        function onOrderBy(option) {
            ctrl.orderBy = option;
        }

        function onInit() {
            ctrl.fields = {
                ID: '',
                IDBook : '',
                Url: 'http://...',
            };
            ctrl.options = [
                {
                    text: 'ID',
                    value: 'ID',
                    type: 'number'
                }, {
                    text: 'Libro ID',
                    value: 'IDBook',
                    type: 'number'
                }, {
                    text: 'Url',
                    value: 'Url',
                    type: 'url'
                }
            ];

            coverphotosService.getcoverphotos()
            .then(function (data) {
                //Consumimos el listado de portadas
                ctrl.coverphotos = data;
            });
        }
    }
})(window.angular);
