(function (angular) {
    'use strict';

    angular
        .module('app.coverphotos')
        .component('coverphotoSummary', coverphotoSummary());

    function coverphotoSummary() {
        var component = {
            templateUrl: '/app/coverphotos/coverphotoSummary/coverphotoSummary.component.html',
            controller: coverphotoSummaryController,
            bindings: {
                coverphoto: '<',
            }
        };
        return component;
    }

    coverphotoSummaryController.$inject = [
        '$location',
        '$window',
        '$route',
        'coverphotosService'
    ];

    function coverphotoSummaryController($location,$window,$route,coverphotosService) { 
        var ctrl = this;
        ctrl.viewcoverphotoDetails    = viewcoverphotoDetails;
        ctrl.deleteItem           = deleteItem;

        function viewcoverphotoDetails(coverphotoId,edit) {
            $location.path('/coverphotos/' + coverphotoId+'/'+edit);
        }

        function deleteItem(coverphotoId){
            if ($window.confirm("¿Quieres eliminar el elemento "+coverphotoId+"?")) {
                coverphotosService.deletecoverphoto(coverphotoId)
                    .then(function (data) {
                        if (data == ""){
                            alert ("El elemento "+coverphotoId+" fue eliminado correctamente, pero la API Rest de pruebas no lo eliminó");
                            //Recargamos la página, debería haber desaparecido el elemento pero no lo hará
                            $route.reload();
                        }
                    console.log(data);
                });
            }
        }
    }
})(window.angular);
