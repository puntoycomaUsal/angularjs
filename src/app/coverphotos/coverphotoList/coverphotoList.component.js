(function (angular) {
    'use strict';

    angular
        .module('app.coverphotos')
        .component('coverphotoList', coverphotoList());

    function coverphotoList() {
        var component = {
            templateUrl: '/app/coverphotos/coverphotoList/coverphotoList.component.html',
            controller: coverphotoListController,
                bindings: {
                    coverphotos: '<',
                    filter:  '<',
                    orderBy: '<'
                }
        };
        return component;
    }

    coverphotoListController.$inject = [
        '$location',
    ];

    function coverphotoListController($location) {
        var ctrl = this;
        ctrl.viewcoverphotoDetails = viewcoverphotoDetails;

        function viewcoverphotoDetails(coverphotoId) {
            $location.path('/coverphotos/' + coverphotoId);
        }
    }
})(window.angular);
