(function (angular) {
    'use strict';

    angular
        .module('app')
        .config(config);

    function config($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'index.template.html',
                css:'index.template.css',
            })
            .when('/books', {
                template: '<book-page></book-page>'
            })
            .when('/books/:bookId/:edit', {
                template: '<book-detail></book-detail>'
            })
            .when('/books/new', {
                template: '<book-new></book-new>'
            })
            .when('/authors', {
                template: '<author-page></author-page>'
            })
            .when('/authors/:authorId/:edit', {
                template: '<author-detail></author-detail>'
            })
            .when('/authors/new', {
                template: '<author-new></author-new>'
            })
            .when('/coverphotos', {
                template: '<coverphoto-page></coverphoto-page>'
            })
            .when('/coverphotos/:coverphotoId/:edit', {
                template: '<coverphoto-detail></coverphoto-detail>'
            })
            .when('/coverphotos/new', {
                template: '<coverphoto-new></coverphoto-new>'
            })
            .otherwise('/');

            $locationProvider.html5Mode(true);
        }
})(window.angular);
