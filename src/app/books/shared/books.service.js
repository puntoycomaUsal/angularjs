(function (angular) {
    'use strict';

    angular
        .module('app.books')
        .factory('booksService', booksService);

    booksService.$inject = [
        '$http'
    ];

    function booksService($http) {
        var urlRoot     = 'https://fakerestapi.azurewebsites.net/';
        var urlRootApi  = 'https://fakerestapi.azurewebsites.net/api/';

        var service = {
            getbook             : getbook,
            getbooks            : getbooks,
            getbookauthors      : getbookauthors,
            getbookcoverphoto   : getbookcoverphoto,
            deletebook          : deletebook,
            editbook            : editbook,
            newbook             : newbook,
        };

        return service;

        function getbooks() {
            return $http.get(urlRootApi + 'Books')
            .then(complete)
            .catch(failed);
        }

        function getbook(bookId) {
            return $http.get(urlRootApi + 'Books/' + bookId)
                .then(complete)
                .catch(failed);
        }

        function getbookauthors(bookId) {
            return $http.get(urlRoot + 'authors/Books/' + bookId)
                .then(complete)
                .catch(failed);
        }

        function getbookcoverphoto(bookId) {
            return $http.get(urlRoot + 'books/covers/' + bookId)
              .then(complete)
              .catch(failed);
          }

        function deletebook(bookId){
            return $http.delete(urlRootApi + 'Books/' + bookId)
                .then(complete)
                .catch(failed);
        }

        function editbook(book){
            return $http.put(urlRootApi + 'Books/' +book.ID,book)
                .then(complete)
                .catch(failed);
        }

        function newbook(book){
            return $http.post(urlRootApi + 'Books/',book)
                .then(complete)
                .catch(failed);
        }

        function complete(response) {
            return response.data;
        }

        function failed(error) {
            console.error(error.data);
        }
    }
})(window.angular);