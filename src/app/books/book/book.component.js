(function (angular) {
    'use strict';

    angular
        .module('app.books')
        .component('book', book());

    function book() {
        var component = {
            templateUrl: '/app/books/book/book.component.html',
            controller: bookController,
            bindings: {
                book: '<',
                edit: '<',
            }
        };
        return component;
    }
    
    bookController.$inject = [
        '$routeParams',
        'booksService',
        '$location',
    ];
    
    function bookController($routeParams,booksService,$location) {
        var ctrl        = this;
        ctrl.$onInit    = onInit;
        ctrl.editForm   = editForm;
    
        function onInit() {
            booksService.getbook($routeParams.bookId)
                .then(function (data) {
                ctrl.date  = new Date(data.PublishDate);
            });
        }

        function editForm(){
            ctrl.book.PublishDate   = ctrl.date;

            booksService.editbook(ctrl.book)
                .then(function (data) {
                    alert ("El elemento fue editado correctamente, se muestra en consola la respuesta");
                    console.log(data);
                    $location.path('/books');
            });
        }

    }
})(window.angular);
