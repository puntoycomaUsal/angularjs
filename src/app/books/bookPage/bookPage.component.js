(function (angular) {
    'use strict';

    angular
        .module('app.books')
        .component('bookPage', bookPage());

    function bookPage() {
        var component = {
            templateUrl: '/app/books/bookPage/bookPage.component.html',
            controller: bookPageController
        };
        return component;
    }

    bookPageController.$inject = [
        'booksService',
    ];

    function bookPageController(booksService) {
        var ctrl        = this;
        ctrl.onFilter   = onFilter;
        ctrl.onOrderBy  = onOrderBy;
        ctrl.$onInit    = onInit;

        function onFilter(filter) {
            ctrl.filter = filter;
        }

        function onOrderBy(option) {
            ctrl.orderBy = option;
        }

        function onInit() {
            ctrl.fields = {
                ID: '',
                Title : '',
                Excerpt:'',
                PageCount: '',
                PublishDate:'',
            };
            ctrl.options = [
                {
                    text: 'ID',
                    value: 'ID',
                    type: 'number'
                }, {
                    text: 'Titulo',
                    value: 'Title',
                    type: 'text'

                }, {
                    text: 'Resumen',
                    value: 'Excerpt',
                    type: 'text'
                }, {
                    text: 'Páginas Totales',
                    value: 'PageCount',
                    type: 'number'
                }, {
                    text: 'Fecha Publicación',
                    value: 'PublishDate',
                    type: 'date'
                }
            ];

            booksService.getbooks()
            .then(function (data) {
                //Consumimos el listado de books
                ctrl.books = data;
            });
        }
    }
})(window.angular);
