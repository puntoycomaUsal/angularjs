(function (angular) {
  'use strict';

  angular
    .module('app.books')
    .component('bookDetail', bookDetail());

  function bookDetail() {
    var component = {
      templateUrl: '/app/books/bookDetail/bookDetail.component.html',
      controller: bookDetailController
    };
    return component;
  }

  bookDetailController.$inject = [
    '$routeParams',
    'booksService'
  ];

  function bookDetailController($routeParams, booksService) {
    var ctrl        = this;
    ctrl.$onInit    = onInit;

    function onInit() {
        ctrl.edit    = $routeParams.edit;

        booksService.getbook($routeParams.bookId)
            .then(function (data) {
                ctrl.book = data;
        });

        booksService.getbookauthors($routeParams.bookId)
            .then(function (data) {
                ctrl.authors = data;
        });

        booksService.getbookcoverphoto($routeParams.bookId)
        .then(function (data) {
            ctrl.coverphotos = data;
        });
    }

  }
})(window.angular);
