(function (angular) {
    'use strict';

    angular
        .module('app.books')
        .component('bookSummary', book());

    function book() {
        var component = {
            templateUrl: '/app/books/bookSummary/bookSummary.component.html',
            controller: bookSummaryController,
            bindings: {
                book: '<',
            }
        };
        return component;
    }

    bookSummaryController.$inject = [
        '$location',
        '$window',
        '$route',
        'booksService'
    ];

    function bookSummaryController($location,$window,$route,booksService) { 
        var ctrl = this;
        ctrl.viewbookDetails    = viewbookDetails;
        ctrl.showSummary        = showSummary;
        ctrl.deleteItem         = deleteItem;

        function viewbookDetails(bookId,edit) {
            $location.path('/books/' + bookId+'/'+edit);
        }

        function showSummary (text,start,end){
            return text.substring(start, end)+' ...';
        }

        function deleteItem(bookId){
            if ($window.confirm("¿Quieres eliminar el elemento "+bookId+"?")) {
                booksService.deletebook(bookId)
                    .then(function (data) {
                        if (data == ""){
                            alert ("El elemento "+bookId+" fue eliminado correctamente, pero la API Rest de pruebas no lo eliminó");
                            //Recargamos la página, debería haber desaparecido el elemento pero no lo hará
                            $route.reload();
                        }
                    console.log(data);
                });
            }
        }
    }
})(window.angular);
