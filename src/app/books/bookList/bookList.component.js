(function (angular) {
    'use strict';

    angular
        .module('app.books')
        .component('bookList', bookList());

    function bookList() {
        var component = {
            templateUrl: '/app/books/bookList/bookList.component.html',
            controller: bookListController,
                bindings: {
                    books: '<',
                    filter:  '<',
                    orderBy: '<'
                }
        };
        return component;
    }

    function bookListController() {
    }
})(window.angular);
