(function (angular) {
    'use strict';

    angular
        .module('app.books')
        .component('bookNew', bookNew());

    function bookNew() {
        var component = {
            templateUrl: '/app/books/bookNew/bookNew.component.html',
            controller: bookNewController,
        };
        return component;
    }
    
    bookNewController.$inject = [
        'booksService',
        '$location',
    ];
    
    function bookNewController(booksService,$location) {
        var ctrl        = this;
        ctrl.$onInit    = onInit;
        ctrl.newForm    = newForm;

        function onInit() {
            ctrl.book       = {
                /*
                Campo ID no se muestra en formulario, ya que para evitar choques
                con otros IDs deberá asignarlo el sistema remoto.
                */
                ID              : 0, 
                Title           :'',
                PublishDate     : new Date(),
                PageCount       : 0,
                Excerpt         : '',
                Description     : '',
            }
        }

        function newForm(){
            booksService.newbook(ctrl.book)
                .then(function (data) {
                    alert ("El elemento fue creado correctamente, se muestra en consola la respuesta");
                    console.log(data);
                    $location.path('/books');
            });
        }

    }
})(window.angular);
