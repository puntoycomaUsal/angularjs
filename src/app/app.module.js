(function (angular) {
    'use strict';

    angular
        .module('app', [
            'app.menu',
            'app.books',
            'app.authors',
            'app.coverphotos',
            'ngRoute'
        ]);
})(window.angular);
