(function (angular) {
    'use strict';

    angular
        .module('app.menu')
        .component('appMenu', menu());

    function menu() {
        var component = {
            templateUrl: '/app/menu/menu/menu.component.html',
            controller: MenuController,
        };
        return component;
    }

    function MenuController() { }
})(window.angular);
