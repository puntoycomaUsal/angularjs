(function (angular) {
    'use strict';

    angular
        .module('app.authors')
        .component('authorPage', authorPage());

    function authorPage() {
        var component = {
            templateUrl: '/app/authors/authorPage/authorPage.component.html',
            controller: authorPageController
        };
        return component;
    }

    authorPageController.$inject = [
        'authorsService',
    ];

    function authorPageController(authorsService) {
        var ctrl        = this;
        ctrl.onFilter   = onFilter;
        ctrl.onOrderBy  = onOrderBy;
        ctrl.$onInit    = onInit;

        function onFilter(filter) {
            ctrl.filter = filter;
        }

        function onOrderBy(option) {
            ctrl.orderBy = option;
        }

        function onInit() {
            ctrl.fields = {
                ID: '',
                IDBook : '',
                FirstName: '',
                LastName: '',
            };
            ctrl.options = [
                {
                    text: 'ID',
                    value: 'ID',
                    type: 'number'
                }, {
                    text: 'Libro ID',
                    value: 'IDBook',
                    type: 'number'
                }, {
                    text: 'Nombre',
                    value: 'FirstName',
                    type: 'text'
                }, {
                    text: 'Apellidos',
                    value: 'LastName',
                    type: 'text'
                }
            ];

            authorsService.getauthors()
            .then(function (data) {
                //Consumimos el listado de autores
                ctrl.authors = data;
            });
        }
    }
})(window.angular);
