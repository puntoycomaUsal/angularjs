(function (angular) {
  'use strict';

  angular
    .module('app.authors')
    .component('authorDetail', authorDetail());

  function authorDetail() {
    var component = {
      templateUrl: '/app/authors/authorDetail/authorDetail.component.html',
      controller: authorDetailController
    };
    return component;
  }

  authorDetailController.$inject = [
    '$routeParams',
    'authorsService'
  ];

  function authorDetailController($routeParams, authorsService) {
    var ctrl = this;
    ctrl.$onInit = onInit;

    function onInit() {
        ctrl.edit    = $routeParams.edit;

        authorsService.getauthor($routeParams.authorId)
            .then(function (data) {
                ctrl.author = data;
        });
    }

  }
})(window.angular);
