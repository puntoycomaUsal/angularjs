(function (angular) {
    'use strict';

    angular
        .module('app.authors')
        .component('authorSummary', authorSummary());

    function authorSummary() {
        var component = {
            templateUrl: '/app/authors/authorSummary/authorSummary.component.html',
            controller: authorSummaryController,
            bindings: {
                author: '<',
            }
        };
        return component;
    }

    authorSummaryController.$inject = [
        '$location',
        '$window',
        '$route',
        'authorsService'
    ];

    function authorSummaryController($location,$window,$route,authorsService) { 
        var ctrl = this;
        ctrl.viewauthorDetails    = viewauthorDetails;
        ctrl.deleteItem           = deleteItem;

        function viewauthorDetails(authorId,edit) {
            $location.path('/authors/' + authorId+'/'+edit);
        }

        function deleteItem(authorId){
            if ($window.confirm("¿Quieres eliminar el elemento "+authorId+"?")) {
                authorsService.deleteauthor(authorId)
                    .then(function (data) {
                        if (data == ""){
                            alert ("El elemento "+authorId+" fue eliminado correctamente, pero la API Rest de pruebas no lo eliminó");
                            //Recargamos la página, debería haber desaparecido el elemento pero no lo hará
                            $route.reload();
                        }
                    console.log(data);
                });
            }
        }
    }
})(window.angular);
