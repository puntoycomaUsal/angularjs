(function (angular) {
  'use strict';

  angular
    .module('app.authors')
    .factory('authorsService', authorsService);

  authorsService.$inject = [
    '$http'
  ];

  function authorsService($http) {
    var urlRoot     = 'https://fakerestapi.azurewebsites.net/';
    var urlRootApi  = 'https://fakerestapi.azurewebsites.net/api/';

    var service = {
      getauthor     : getauthor,
      getauthors    : getauthors,
      deleteauthor  : deleteauthor,
      editauthor    : editauthor,
      newauthor     : newauthor,
    };

    return service;

    function getauthors() {
      return $http.get(urlRootApi + 'Authors')
        .then(completeGetauthors)
        .catch(failed);
    }

    function completeGetauthors(response) {
      return response.data;
    }

    function getauthor(authorId) {
      return $http.get(urlRootApi + 'Authors/' + authorId)
        .then(complete)
        .catch(failed);
    }

    function deleteauthor(authorId){
        return $http.delete(urlRootApi + 'Authors/' + authorId)
            .then(complete)
            .catch(failed);
    }

    function editauthor(author){
        return $http.put(urlRootApi + 'Authors/' +author.ID,author)
            .then(complete)
            .catch(failed);
    }

    function newauthor(author){
        return $http.post(urlRootApi + 'Authors/',author)
            .then(complete)
            .catch(failed);
    }

    function complete(response) {
      return response.data;
    }

    function failed(error) {
      console.error(error.data);
    }
  }
})(window.angular);