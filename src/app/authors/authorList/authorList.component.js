(function (angular) {
    'use strict';

    angular
        .module('app.authors')
        .component('authorList', authorList());

    function authorList() {
        var component = {
            templateUrl: '/app/authors/authorList/authorList.component.html',
            controller: authorListController,
                bindings: {
                    authors: '<',
                    filter:  '<',
                    orderBy: '<'
                }
        };
        return component;
    }

    authorListController.$inject = [
        '$location',
    ];

    function authorListController($location) {
        var ctrl = this;
        ctrl.viewauthorDetails = viewauthorDetails;

        function viewauthorDetails(authorId) {
            $location.path('/authors/' + authorId);
        }
    }
})(window.angular);
