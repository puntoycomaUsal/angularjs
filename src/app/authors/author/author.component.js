(function (angular) {
    'use strict';

    angular
        .module('app.authors')
        .component('author', author());

    function author() {
        var component = {
            templateUrl: '/app/authors/author/author.component.html',
            controller: authorController,
            bindings: {
                author: '<',
                edit: '<',
            }
        };
        return component;
    }

    authorController.$inject = [
        '$routeParams',
        'authorsService',
        '$location',
    ];

    function authorController($routeParams,authorsService,$location) {
        var ctrl        = this;
        ctrl.$onInit    = onInit;
        ctrl.editForm   = editForm;
    
        function onInit() {
            authorsService.getauthor($routeParams.authorId)
                .then(function (data) {
                //ctrl.date  = new Date(data.PublishDate);
            });
        }

        function editForm(){
            //ctrl.author.PublishDate   = ctrl.date;

            authorsService.editauthor(ctrl.author)
                .then(function (data) {
                    alert ("El elemento fue editado correctamente, se muestra en consola la respuesta");
                    console.log(data);
                    $location.path('/authors');
            });
        }
    }
})(window.angular);
