(function (angular) {
    'use strict';

    angular
        .module('app.authors')
        .component('authorNew', authorNew());

    function authorNew() {
        var component = {
            templateUrl: '/app/authors/authorNew/authorNew.component.html',
            controller: authorNewController,
        };
        return component;
    }
    
    authorNewController.$inject = [
        'authorsService',
        '$location',
    ];
    
    function authorNewController(authorsService,$location) {
        var ctrl        = this;
        ctrl.$onInit    = onInit;
        ctrl.newForm    = newForm;

        function onInit() {
            /*
            Campo ID no se muestra en formulario, ya que para evitar choques
            con otros IDs deberá asignarlo el sistema remoto.
            */
            ctrl.author      = {
                ID           :0, 
                IDBook       :0,
                FirstName    :'',
                LastName     :'',
            }
        }

        function newForm(){
            authorsService.newauthor(ctrl.author)
                .then(function (data) {
                    alert ("El elemento fue creado correctamente, se muestra en consola la respuesta");
                    console.log(data);
                    $location.path('/authors');
            });
        }

    }
})(window.angular);
